import pyglet
import random
import sorters
from pyglet.window import key


# Window
window = pyglet.window.Window(width=1200, height=800,
                              caption="Sorting Program", resizable=True)
window.set_location(400, 100)

# Images
blue_cell = pyglet.image.load('images/cellb.png')
yellow_cell = pyglet.image.load('images/yellow_cellb.png')
red_cell = pyglet.image.load('images/red_cellb.png')
background_image = pyglet.image.load('images/background_3.jpg')
background = pyglet.sprite.Sprite(background_image)

# Global variables
numbers = random.sample(range(1, 20), 10) + random.sample(range(-20, 20), 10)
numbers_sorted = sorted(numbers)
update_speed = 1./30
number_sprites = []
label_sprites = []
selecting = []
finished = []
i_swap, j_swap = 0, 0
swap_mode = False
sort_mode = 'bubble'
left_bar_destination, right_bar_destination = None, None
states = []
not_sorted = True
selecting_list = []
step_by_step_mode = True


@window.event
def on_draw():
    window.clear()

    # Draw background
    background.draw()

    # Draw numbers
    for col in number_sprites:
        for e in col:
            e.draw()

    # Draw number's labels
    for sprite in label_sprites:
        sprite.draw()


@window.event
def on_key_press(symbol, modifiers):
    global step_by_step_mode
    if symbol == key.ENTER:
        step_by_step_mode = False
        main()
    if symbol == key.RIGHT:
        step_by_step_mode = True
        pyglet.clock.schedule_interval(update, update_speed)


def update_sprites():
    # Update number and label sprites
    global number_sprites, label_sprites
    margin = window.width//len(numbers)//2
    width = margin//1.5

    number_sprites = []
    label_sprites = []
    for index, number in enumerate(numbers):
        height = 1 + numbers_sorted.index(number)
        col = create_bar(index, number, width, height)
        number_sprites.append(col)
        width += (window.width-margin)//len(numbers)


def create_bar(index, number, width, height):
    col = []

    for k in range(height):
        if index in selecting:
            col.append(pyglet.sprite.Sprite(red_cell, x=width,
                                            y=k * 32 + 10))
        elif index in finished:
            col.append(pyglet.sprite.Sprite(yellow_cell, x=width,
                                            y=k * 32 + 10))
        else:
            col.append(pyglet.sprite.Sprite(blue_cell, x=width,
                                            y=k * 32 + 10))
        col.append(
            pyglet.text.Label(
                str(number),
                font_size=18,
                font_name='Arial',
                bold=True,
                color=(18, 2, 249, 255),
                x=5+width,
                y=height*32+20)
        )
    return col


def swap_bar():
    global swap_mode, numbers, left_bar_destination, right_bar_destination

    finish_left = False
    finish_right = False

    left_bar = number_sprites[i_swap]
    right_bar = number_sprites[j_swap]

    if left_bar_destination is None or right_bar_destination is None:
        left_bar_destination = right_bar[0].x
        right_bar_destination = left_bar[0].x

    if right_bar[0].x > right_bar_destination:
        for e in right_bar:
            e.x -= abs(right_bar_destination - e.x)//10 + 5
    else:
        finish_right = True

    if left_bar[0].x < left_bar_destination:
        for e in left_bar:
            e.x += abs(left_bar_destination - e.x)//10 + 5
    else:
        finish_left = True

    if finish_left and finish_right:
        swap_mode = False
        left_bar_destination, right_bar_destination = None, None
        numbers[i_swap], numbers[j_swap] = numbers[j_swap], numbers[i_swap]


def bubble_and_insertion_sort():
    global states, not_sorted, swap_mode, i_swap, j_swap, finished, \
        selecting, selecting_list

    if selecting_list:
        if sort_mode == 'bubble':
            if states and selecting_list[0] == states[0]:
                i_swap, j_swap, finished = states.pop(0)
                selecting = [i_swap, j_swap]
                swap_mode = True
                return
            else:
                i_swap, j_swap, finished = selecting_list.pop(0)
                selecting = [i_swap, j_swap]
                return
        else:
            if states and selecting_list[0][0] == states[0][0]:
                i_swap, j_swap, finished = states.pop(0)
                selecting = [i_swap, j_swap]
                swap_mode = True
                return
            else:
                selected, finished = selecting_list.pop(0)
                selecting = [selected]
                return
    else:
        if not_sorted:
            if sort_mode == 'bubble':
                sorters.bubble_sort(numbers.copy())
            elif sort_mode == 'insert':
                sorters.insertion_sort(numbers.copy())
            states = sorters.states
            selecting_list = sorters.selecting
            not_sorted = False
        else:
            selecting = []
            finished = list(range(len(numbers)))


def quick_sort():
    global states, i_swap, j_swap, swap_mode, selecting, finished,\
        not_sorted, selecting_list

    if states:
        i, j = states.pop(0)
        if i != -1:
            i_swap, j_swap = i, j
            selecting = [i_swap, j_swap]
            swap_mode = True
        else:
            selecting = list(range(j[1], j[2]+1))
            finished = [j[1]]
            return
    else:
        if not_sorted:
            sorters.quick_sort(numbers.copy())
            states = sorters.states
            selecting_list = sorters.selecting
            not_sorted = False
        else:
            selecting = []
            finished = list(range(len(numbers)))


def merge_sort():
    global selecting, finished, not_sorted, selecting_list,\
        numbers, states

    if states:
        state = states.pop(0)
        sel = selecting_list.pop(0)

        numbers = []
        for index, part in enumerate(state):
            if index == sel[0]:
                if sel[0] == sel[1]:
                    selecting = []
                    finished = list(range(len(numbers), len(numbers)+len(part))
                                    )
                else:
                    selecting = list(range(len(numbers), len(numbers)+len(part)
                                           ))
                    finished = list(range(selecting[-1]+1,
                                          selecting[-1]+1+len(state[index+1])))
            for number in part:
                numbers.append(number)
        return
    else:
        if not_sorted:
            m = sorters.MergeSorter()
            m.sort(numbers.copy())
            states = m.states
            selecting_list = m.selecting_list
            not_sorted = False
        else:
            selecting = []
            finished = list(range(len(numbers)))


def merge_sort_in_place():
    global selecting, finished, not_sorted, selecting_list, \
        numbers, states, i_swap, j_swap, swap_mode

    if states:
        state = states.pop(0)
        sel = selecting_list.pop(0)
        mid = len(sel)//2
        selecting = sel[:mid]
        finished = sel[mid:]
        i_swap, j_swap = state
        swap_mode = True
        return
    else:
        if not_sorted:
            m = sorters.MergeSorter()
            m.merge_in_place(numbers.copy())
            states = m.states
            selecting_list = m.selecting_list
            not_sorted = False
        else:
            selecting = []
            finished = list(range(len(numbers)))


def update(dt):
    global numbers, swap_mode

    if not swap_mode:
        if sort_mode in ('bubble', 'insert'):
            bubble_and_insertion_sort()
        elif sort_mode == 'quick':
            quick_sort()
        elif sort_mode == 'merge':
            merge_sort()
        else:
            merge_sort_in_place()
        update_sprites()
        if step_by_step_mode:
            pyglet.clock.unschedule(update)
    else:
        swap_bar()


def main():
    window.set_caption(sort_mode.capitalize())

    speed = update_speed if sort_mode != 'merge' else 1.
    if not step_by_step_mode:
        pyglet.clock.schedule_interval(update, speed)
    else:
        update(1.)
    pyglet.app.run()


if __name__ == '__main__':
    main()
