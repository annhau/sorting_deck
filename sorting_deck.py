#!/usr/bin/env python3
import argparse
from sorters import bubble_sort, quick_sort, MergeSorter, insertion_sort


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('numbers', metavar='N', type=int, nargs='+',
                        help='an integer for the list to sort')
    parser.add_argument('--algo', default='bubble',
                        help='specify which algorithm '
                             'to use for sorting among '
                             '[bubble|insert|quick|merge],'
                             ' default bubble')
    parser.add_argument('--gui', action='store_true',
                        help='visualise the algorithm in GUI mode')

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_arguments()

    if args.gui:
        if not 1 <= len(args.numbers) <= 15:
            print('Input too large')
            exit()
        else:
            import sorting_gui as gui
            gui.numbers = args.numbers
            gui.numbers_sorted = sorted(args.numbers)
            gui.sort_mode = args.algo
            gui.main()

    if args.algo == 'bubble':
        bubble_sort(args.numbers)
    elif args.algo == 'quick':
        quick_sort(args.numbers)
    elif args.algo == 'merge':
        MergeSorter().recursive_sort(args.numbers)
    elif args.algo == 'insert':
        insertion_sort(args.numbers)
