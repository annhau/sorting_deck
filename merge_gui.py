import pyglet
import random
from sorters import MergeSorter
import sorters


window = pyglet.window.Window(width=1200, height=700,
                              caption="Sorting Program", resizable=True)
window.set_location(100, 10)

numbers = random.sample(range(1, 20), 15)
m = MergeSorter()
m.recursive_sort(numbers)
states = m.merge_states
last_states = states[-1]

background_image = pyglet.image.load('images/background_2.jpg')
background = pyglet.sprite.Sprite(background_image)


@window.event
def on_draw():
    window.clear()
    # background.draw()

    # Draw state
    if states:
        state = states.pop(0)
        partition_one, partition_two, merged_partition = state

        if not merged_partition:
            pyglet.text.Label("Status: Start merging",
                              font_size=24,
                              bold=True,
                              color=(0, 155, 24, 255),
                              x=window.width//2,
                              y=window.height-50,
                              anchor_x='center',
                              anchor_y='center').draw()
        elif not partition_one and not partition_two:
            pyglet.text.Label("Status: Done.",
                              font_size=24,
                              bold=True,
                              color=(0, 155, 24, 255),
                              x=window.width//2,
                              y=window.height-50,
                              anchor_x='center',
                              anchor_y='center').draw()
        else:
            pyglet.text.Label("Status: Merging...",
                              font_size=24,
                              bold=True,
                              color=(0, 155, 24, 255),
                              x=window.width//2,
                              y=window.height-50,
                              anchor_x='center',
                              anchor_y='center').draw()

        pyglet.text.Label("Partition one",
                          font_size=24,
                          bold=True,
                          color=(0, 0, 235, 255),
                          x=window.width//5,
                          y=window.height*2//3+100,
                          anchor_x="center").draw()
        pyglet.text.Label('  '.join(map(str, partition_one)),
                          font_size=24,
                          bold=True,
                          color=(0, 0, 235, 255),
                          x=window.width//5,
                          y=window.height*2//3,
                          anchor_x="center").draw()

        pyglet.text.Label("Partition two",
                          font_size=24,
                          bold=True,
                          color=(0, 0, 235, 255),
                          x=window.width*3//5+100,
                          y=window.height*2//3+100,
                          anchor_x="center").draw()
        pyglet.text.Label('  '.join(map(str, partition_two)),
                          font_size=24,
                          bold=True,
                          color=(0, 16, 255, 255),
                          x=window.width*3//5,
                          y=window.height*2//3,
                          anchor_x="left").draw()

        pyglet.text.Label("New Partition",
                          font_size=24,
                          bold=True,
                          color=(0, 255, 58, 255),
                          x=window.width//2,
                          y=window.height//3+100,
                          anchor_x='center',
                          anchor_y='center').draw()
        pyglet.text.Label('   '.join(map(str, merged_partition)),
                          font_size=24,
                          bold=True,
                          color=(0, 255, 58, 255),
                          x=window.width//2,
                          y=window.height//3,
                          anchor_x='center',
                          anchor_y='center').draw()

    else:
        pyglet.text.Label("Status: Finish.",
                          font_size=24,
                          bold=True,
                          color=(0, 155, 24, 255),
                          x=window.width//2,
                          y=window.height-50,
                          anchor_x='center',
                          anchor_y='center').draw()
        pyglet.text.Label('   '.join(map(str, last_states[2])),
                          font_size=24,
                          bold=True,
                          color=(0, 255, 58, 255),
                          x=window.width//2,
                          y=window.height//3,
                          anchor_x='center',
                          anchor_y='center').draw()


def update(dt):
    pass


def main():
    pyglet.clock.schedule_interval(update, 1/2)
    pyglet.app.run()


if __name__ == '__main__':
    main()
