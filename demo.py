import subprocess
import threading


class GuiThread (threading.Thread):
    def __init__(self, numbers, algo):
        threading.Thread.__init__(self)
        self.numbers = list(map(str, numbers))
        self.algo = algo

    def run(self):
        args = ['python', 'sorting_deck.py', '--gui'] + self.numbers + ['--algo'] + [self.algo]
        subprocess.call(args)


if __name__ == '__main__':
    import random
    x = random.sample(range(20), 15)
    t1 = GuiThread(x.copy(), 'quick')
    t2 = GuiThread(x.copy(), 'merge-in-place')

    t1.start()
    t2.start()
