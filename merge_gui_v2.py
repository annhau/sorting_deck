import pyglet
import random
from sorters import MergeSorter
import sorters


window = pyglet.window.Window(width=1200, height=700,
                              caption="Sorting Program", resizable=True)
window.set_location(100, 10)

numbers = random.sample(range(1, 20), 15)
m = MergeSorter()
m.sort(numbers)
states = m.states
selecting = m.selecting_list
last_states = states[-1]

background_image = pyglet.image.load('images/background_2.jpg')
background = pyglet.sprite.Sprite(background_image)


@window.event
def on_draw():
    window.clear()
    # background.draw()

    # Draw state
    if states:
        state = states.pop(0)
        width = 20
        color = 0
        print(state)
        for part in state:
            pyglet.text.Label(' '.join(map(str, part)),
                              font_size=24,
                              bold=True,
                              color=(255, color, 255-color, 255),
                              x=width,
                              y=window.height//2
                              ).draw()
            color += 15
            width += len(part)*72

    else:
        pyglet.text.Label('   '.join(map(str, last_states[0])),
                          font_size=24,
                          bold=True,
                          color=(0, 255, 58, 255),
                          x=window.width//2,
                          y=window.height//3,
                          anchor_x='center',
                          anchor_y='center').draw()


def update(dt):
    pass


def main():
    pyglet.clock.schedule_interval(update, 1./2)
    pyglet.app.run()


if __name__ == '__main__':
    main()
