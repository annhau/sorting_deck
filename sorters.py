import threading
import subprocess


states = []
selecting = []


def quick_sort(numbers, first=None, last=None):
    global states, selecting
    """
    Best and average: O(n log n)
    Worst: O(n^2)
    Worst case happens when maximum value always be choosed as pivot
    """
    # Init first and last value if not specified
    if first is None or last is None:
        first = 0
        last = len(numbers) - 1

    # If first >= last, stop
    if first >= last:
        return

    middle = (first+last)//2
    # Choose pivot in the middle
    pivot = numbers[middle]
    # Move pivot to the start
    numbers[middle], numbers[first] = numbers[first], numbers[middle]

    # this contains pivot and selecting partition
    states += [(-1, (middle, first, last))]
    # this contains things to swap
    states += [(first, middle)]
    selecting += [(first, middle)]

    # Consider from second to last
    i = first+1
    j = last
    done = False
    while not done:
        # Find first left mark that > pivot
        while i <= j and numbers[i] <= pivot:
            selecting += [(i, j)]
            i = i + 1
        # Find first right mark that < pivot
        while numbers[j] >= pivot and j >= i:
            selecting += [(i, j)]
            j = j - 1
        # If marks cross each others, stop
        if j < i:
            done = True
        else:
            # Swap their values
            numbers[i], numbers[j] = numbers[j], numbers[i]
            states += [(i, j)]
            selecting += [(i, j)]

    numbers[j], numbers[first] = numbers[first], numbers[j]
    states += [(first, j)]
    selecting += [(first, j)]

    # Print pivot and state of list
    print('P: %d' % pivot)
    print(' '.join(map(str, numbers)))

    # Recur with 2 small partitions with break point is where they cross
    quick_sort(numbers, first, j - 1)
    quick_sort(numbers, j + 1, last)


def bubble_sort(numbers):
    global states, selecting
    """
    Repeatedly swapping the adjacent elements if they are in wrong order
    until no change was made.
    Best case - when the list is already sorted: O(n)
    Worst and average: O(n^2)
    """
    n = len(numbers)

    swap_made = True
    while swap_made:
        swap_made = False
        for i in range(n - 1):
            selecting += [(i, i+1, list(range(n, len(numbers))))]
            if numbers[i] > numbers[i + 1]:
                numbers[i], numbers[i + 1] = numbers[i + 1], numbers[i]
                states += [(i, i + 1, list(range(n, len(numbers))))]
                swap_made = True
                print(' '.join([str(number) for number in numbers]))

        # Last element is already in place
        n -= 1

    return numbers


def insertion_sort(arr):
    global states, selecting

    for i in range(0, len(arr)):
        insert = False
        selecting += [(i, list(range(0, i+1)))]
        for j in range(i, 0, -1):
            if arr[j-1] > arr[j]:
                arr[j-1], arr[j] = arr[j], arr[j-1]
                states += [(j - 1, j, list(range(0, i+1)))]
                selecting += [(j-1, list(range(0, i+1)))]
                insert = True

        if insert:
            print(' '.join(map(str, arr)))


class MergeSorter(object):
    merge_states = []
    states = []
    selecting_list = []
    cost = 0
    """
    1. Divide the unsorted list into n sublists, each containing 1 element
        (a list of 1 element is considered sorted).
    2. Repeatedly merge sublists to produce new sorted sublists until
        there is only 1 sublist remaining. This will be the sorted list.
    Worst - Average - Best : O(n log n)
    """

    def merge(self, arr1, arr2):
        # Merge 2 sorted array into one sorted array
        arr = []

        i, j = 0, 0
        l1, l2 = len(arr1), len(arr2)
        self.merge_states.append((arr1.copy(), arr2.copy(), arr.copy()))
        while i < l1 and j < l2:
            if arr1[i] < arr2[j]:
                arr.append(arr1[i])
                self.merge_states.append(
                    (arr1[i+1:].copy(), arr2[j:].copy(), arr.copy())
                )
                i += 1
            else:
                arr.append(arr2[j])
                self.merge_states.append(
                    (arr1[i:].copy(), arr2[j+1:].copy(), arr.copy())
                )
                j += 1

        if i < l1:
            arr += arr1[i:]
            self.merge_states.append(([], [], arr.copy()))
        if j < l2:
            arr += arr2[j:]
            self.merge_states.append(([], [], arr.copy()))

        return arr

    def recursive_sort(self, arr):
        if len(arr) > 1:
            middle = len(arr)//2
            left_half = self.recursive_sort(arr[:middle])
            right_half = self.recursive_sort(arr[middle:])

            arr = self.merge(left_half, right_half)
            print(' '.join(map(str, arr)))
            return arr
        else:
            return arr

    def sort(self, arr):
        # split one size n array into n size 1 arrays
        arr = [[x] for x in arr]
        self.states.append(arr.copy())

        # merge
        while len(arr) > 1:
            # while they're not merged into one
            i = 0
            while i+1 < len(arr):
                self.selecting_list.append([i, i + 1])
                self.selecting_list.append([i, i])
                arr[i] = self.merge(arr[i], arr.pop(i+1))
                i += 1
                self.states.append(arr.copy())
                self.states.append(arr.copy())

        self.states.pop(-1)
        return arr[0]

    def merge_in_place(self, arr):
        """
        Merge 2 partitions each time, size starts with 1 and increase 2
        times every step.
        """
        size = 1
        while size <= len(arr):
            # for every 2 partitions
            for h in range(0, len(arr), size * 2):
                # Left boundary and right boundary, if r > len, then r = len
                left, right = h, min(len(arr), h + 2 * size)
                # Middle boundary, that where we split 2 partitions
                mid = h + size
                i, j = left, mid
                while i < mid and j < right:
                    if arr[i] <= arr[j]:
                        i += 1
                    else:
                        # Move arr[j] to the i
                        for k in range(j, i, -1):
                            self.states += [(k-1, k)]
                            self.selecting_list += [list(range(left, right))]
                        arr.insert(i, arr.pop(j))
                        i, mid, j = i + 1, mid + 1, j + 1

            size *= 2

        return arr
