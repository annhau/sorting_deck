# Sorting Deck
A project to visualize sorting algorithms using **pyglet**.

## Installation
> pip install -r requirements.txt


## Project Structure

**sorting_deck.py**: Main program, handle arguments 

**sorters.py**: Implementation of sorting algorithms and the state to update pyglet UI.

**sorting_gui.py**: Handle Pyglet UI


## Run
> python sorting_deck.py --help

or a quick demo:
> python demo.py

Press ENTER to start auto mode, or press RIGHT key to see each step.



## Demo
The visualization shows which elements are selecting and how they are swapped to perform the sort.
![img_2.png](img_2.png)
![img.png](img.png)
![img_1.png](img_1.png)
![img_3.png](img_3.png)